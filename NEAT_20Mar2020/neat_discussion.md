Comments:
1. Pole balancing is old benchmark and is largely a solved problem
    - Can be solved with a few nodes in the hidden layer (notice the compact graph produced by NEAT itself)
    - however, it has many variant - (a) with and without velocity (b) with double poles (c) with swing-up task in addition to simply balancing the pole
    - Q: What are the most recent benchmarks in RL/continuous control, and how do they compare in difficulty?
    - A: Here is an ongoing 'RL leaderboard' maintained by openai: https://github.com/openai/gym/wiki/Leaderboard. Should appear that the tasks that have fewer submissions or lower scores are higher on the difficulty scale.
    - Q: What is the next milestone in RL?
    - A: To encode high-dim input (from complex environments); generalisation to different environments - metalearning - domain adaptation
2. Q: GAs are designed to grow from minimal topologies as and when the complexity is needed. Is such a focus on minimal structure required? For instance, we know from deep learning literature that complex DL architectures can be learned successfully.
    - The answer can be given in two parts: 
    - (a) Since GAs implement a rather crude search for optimal parameters (connection weights) it helps the learning process to optimise the weights for low-dimensional structures first before complexifying. 
    - (b) Recent works (e.g. Lottery ticket hypothesis) have shown that even in highly complex NN architectures, a 'small' proportion of 'fortuitously' initialised connections are the main drivers of performance.
3. GAs, in the context of DL, can be useful for exploring the space of hyperparamters for DL instantiation and training. What are some alternatives being explored?
    - There is this from ICML 2014: http://aad.informatik.uni-freiburg.de/papers/14-AUTOML-ExtrapolatingLearningCurves4.pdf
    - A rather simple idea of projecting the learning curves into the future for early identification of promising directions in the hyperparamter space
    - This could be useful in the context of GAs to identify promising innovations
4. Where has NEAT gotten to in today's date?
    - Miikkulainen's talk at the RNN Symposium 2016: https://www.youtube.com/watch?v=Y9ibdHIkgAA
    - Milestones:
        * They have successfully evolved LSTM units for playing poker
        * They have experimented evolving the connections between LSTM units (insight: connecting the memory states of 2 consecutive LSTM unit helps)
        * They have developed schemes for evolving repeatable components/modules of NNs, and then evolving the blueprint of connections between these modules to design complex architectures
    - Catch: 
        * Clearly, evolving mammoth architectures requires huge compute power
        * They suggest to employ the 'darkcycles' - cpus in cafes when they are not in use
        * But actually end up using 2M cpus and 5K gpus from trading companies
5. A successor to NEAT, called FS-NEAT, uses GAs for feature selection. What are some other DL techniques in use?
    - Might be worth looking at a recent in-house research work: https://ecmlpkdd2019.org/downloads/paper/744.pdf
6. Open questions (particularly in context of Neural Architecture Search (NAS))
    - Do we need crossover between topologies to evolve new ones?
    - If yes, how do we ensure that the children get to keep the best of the parents (over and above the simple scheme of retaining common parts or all parts from the fitter parent)
    - How can we design reward so that the innovation is protected (over and above speciation)
    - Do we have a benchmark that is not too easy nor too hard?